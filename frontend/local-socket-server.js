const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http, {
  path: "/event-server",
  cors: ["http://localhost:3000/"],
});

app.use(express.static("public"));

let socketIsActive = false;

io.on("connection", (socket) => {
  console.log("User connected.");

  socket.on("disconnect", () => {
    console.log("User disconnected.");
  });

  socket.onAny((e, data) => {
    console.log("Event:", e);
    console.log("Data from event:", data);

    if (e === "controlReactions") {
      console.log("Inside control Reactions check:", e);
      console.log("Data from control Reactions component:", data);
      const { isDisplay } = data;
      socketIsActive = isDisplay;
    }

    if (e === "resetStats") {
      console.log("Inside reset Reactions Statistics check:", e);
      console.log("Data from reset Reactions Statistics component:", data);
      socket.broadcast.emit(e, data);
    }

    if (e === "setNumberAnimationStart") {
      console.log("Inside reset Reactions Interval check:", e);
      console.log("Data from reset Reactions Interval component:", data);
      socket.broadcast.emit(e, data);
    }

    if (e === "resetEmojisList") {
      console.log("Inside resetEmojisList check:", e);
      console.log("Data from reset EmojisList component:", data);
      socket.broadcast.emit(e, data);
    }

    if (e === "isDisplayStats") {
      console.log("Inside isDisplayStats check:", e);
      console.log("Data from isDisplayStats component:", data);
      socket.broadcast.emit(e, data);
    }

    if (socketIsActive) {
      socket.broadcast.emit(e, data);
      console.log("data:", data, "e:", e);
    }
  });
});

const port = process.env.PORT || 4000;
http.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

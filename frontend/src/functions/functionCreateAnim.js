import {
  additionalAnimationTime,
  numAdditionalEmojis,
} from "../constants/constants";

export function createAnimatedEmojis(
  emojiForAnimation,
  setAnimatedEmojis,
  setPendingAnimation
) {
  setPendingAnimation(true);

  const getRandomTop = (offset) => {
    const minHeight = offset;
    const maxHeight = window.innerHeight - offset;
    return Math.floor(Math.random() * (maxHeight - minHeight)) + minHeight;
  };
  const getRandomLeft = (offset) => {
    const minWidth = offset;
    const maxWidth = window.innerWidth - offset;
    return Math.floor(Math.random() * (maxWidth - minWidth)) + minWidth;
  };

  for (let i = 0; i < numAdditionalEmojis; i++) {
    const newAnimatedEmoji = {
      id: "animatedEmoji-" + Math.floor(Math.random() * 10 ** 17),
      //here you can adjust the location of the animation
      top: getRandomTop(50) * 0.8,
      left: getRandomLeft(50) / 2,
      emojisAnimated: emojiForAnimation,
    };

    setAnimatedEmojis((prevState) => [...prevState, newAnimatedEmoji]);
    // console.log(newAnimatedEmoji);
    setTimeout(() => {
      setAnimatedEmojis((prevState) =>
        prevState.filter(
          (emojisAnimated) => emojisAnimated.id !== newAnimatedEmoji.id
        )
      );
    }, additionalAnimationTime);
  }

  console.log("Additional emojis animation created!!!");
}

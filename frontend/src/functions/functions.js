export function createEmoji(msg, setEmojis) {
  const parseData = JSON.parse(msg);
  const emoji = parseData.emoji;
  const user = parseData.user;

  const newEmoji = {
    id: "emoji-" + Math.floor(Math.random() * 10 ** 17),
    top: 0,
    left: Math.floor((Math.random() * window.innerWidth) / 3),
    emoji: emoji,
    user: user,
  };
  setEmojis((prevState) => [...prevState, newEmoji]);
  setTimeout(() => {
    setEmojis((prevState) =>
      prevState.filter((emoji) => emoji.id !== newEmoji.id)
    );
  }, 5000);
}

export function convertToEmojisStat(emojisList) {
  const emojisStat = {};
  emojisList.forEach((emoji) => {
    emojisStat[emoji] = 0;
  });
  return emojisStat;
}

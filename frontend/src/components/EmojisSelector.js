import { useState } from "react";
import { socket } from "../services/socket";
import { emojisAllList } from "../constants/constants";
import { ConfirmButtons } from "./ConfirmButtons";
import {
  Box,
  OutlinedInput,
  InputLabel,
  MenuItem,
  FormControl,
  ListItemText,
  Chip,
  Select,
  Checkbox,
} from "@mui/material";

export function EmojisSelector() {
  const [selectedEmojisList, setSelectedEmojisList] = useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelectedEmojisList(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  function resetEmojisList() {
    socket.emit("resetEmojisList", selectedEmojisList);
  }

  return (
    <div>
      <FormControl>
        <InputLabel id="demo-multiple-checkbox-label">Tag</InputLabel>
        <Select
          labelId="demo-multiple-checkbox-label"
          id="demo-multiple-checkbox"
          multiple
          value={selectedEmojisList}
          onChange={handleChange}
          sx={{
            minWidth: "12rem",
          }}
          input={<OutlinedInput label="Tag" />}
          renderValue={(selected) => (
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                gap: 0.5,
              }}
            >
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
        >
          {emojisAllList.map((emoji) => (
            <MenuItem key={emoji} value={emoji}>
              <Checkbox checked={selectedEmojisList.indexOf(emoji) > -1} />
              <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                <ListItemText primary={emoji} />
              </Box>
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <ConfirmButtons
        buttonVariant="outlined"
        buttonText="Reset Emojis"
        widthMainButton={12}
        confirmedFunction={resetEmojisList}
      />
    </div>
  );
}

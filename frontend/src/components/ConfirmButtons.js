import { useState } from "react";
import { Button, Box } from "@mui/material";

export const ConfirmButtons = ({
  buttonVariant,
  buttonText,
  confirmedFunction,
  widthMainButton,
}) => {
  const [open, setOpen] = useState(false);

  const widthConfirmButtons = `${widthMainButton * 0.36}rem`;

  const handleConfirmResetStat = () => {
    setOpen(false);
    confirmedFunction();
  };

  return (
    <Box>
      {open ? (
        <>
          <Button
            size="small"
            variant={buttonVariant}
            color="primary"
            onClick={handleConfirmResetStat}
            style={{
              marginRight: "1rem",
              minWidth: widthConfirmButtons,
            }}
          >
            Yes
          </Button>
          <Button
            size="small"
            variant={buttonVariant}
            onClick={() => setOpen(false)}
            style={{ minWidth: widthConfirmButtons }}
          >
            No
          </Button>
        </>
      ) : (
        <Button
          style={{ minWidth: `${widthMainButton}rem` }}
          size="small"
          variant={buttonVariant}
          color="primary"
          onClick={() => setOpen(true)}
        >
          {buttonText}
        </Button>
      )}
    </Box>
  );
};

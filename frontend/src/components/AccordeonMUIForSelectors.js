import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { EmojisSelector } from "./EmojisSelector";
import { AnimationNumberSelector } from "./AnimationNumberSelector";


export function AccordeonMUIForSelectors() {
  return (
    <div>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Emojis Settings</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <EmojisSelector />
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>Animation Interval</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <AnimationNumberSelector />
        </AccordionDetails>
      </Accordion>
    </div>
  );
}

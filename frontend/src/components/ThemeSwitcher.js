import * as React from "react";
import { styled } from "@mui/material/styles";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

import { useContext } from "react";
import { ThemeContext } from "../themes/ThemeWrapper";

import SunSVG from "../animation/sunSvgRepoCom.svg";
import MoonSVG from "../animation/moonSvgRepoCom.svg";

const Android12Switch = styled(Switch)(({ theme }) => ({
  padding: 8,
  "& .MuiSwitch-track": {
    borderRadius: 22 / 2,
    "&:before, &:after": {
      content: '""',
      position: "absolute",
      top: "50%",
      transform: "translateY(-50%)",
      width: 16,
      height: 16,
    },
    "&:before": {
      backgroundImage: `url(${SunSVG})`,
      left: 12,
    },
    "&:after": {
      backgroundImage: `url(${MoonSVG})`,
      right: 12,
    },
  },
  "& .MuiSwitch-thumb": {
    boxShadow: "none",
    width: 16,
    height: 16,
    margin: 2,
  },
}));

export function ThemeSwitcher() {
  const { toggleTheme } = useContext(ThemeContext);

  return (
    <FormControlLabel
      control={<Android12Switch defaultChecked />}
      onClick={toggleTheme}
      sx={{ mx: -0.5 }}
    />
  );
}

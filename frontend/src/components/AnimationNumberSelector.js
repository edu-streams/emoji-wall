import { useState, useEffect } from "react";
import { socket } from "../services/socket";

import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";

import { ConfirmButtons } from "./ConfirmButtons";
import { defaultAnimationInterval } from "../constants/constants";

export function AnimationNumberSelector() {
  const [numberAnimationStart, setNumberAnimationStart] = useState(
    defaultAnimationInterval
  );
  // const [open, setOpen] = useState(false);
  const [outlineColor, setOutlineColor] = useState("");

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setNumberAnimationStart(value);
  };

  function resetIntervalAnimation() {
    setOutlineColor("rgb(0,255,0,0.3)");
    socket.emit("setNumberAnimationStart", numberAnimationStart);
    console.log("numberAnimationStart is set:", numberAnimationStart);
    setTimeout(() => {
      setOutlineColor("");
    }, 1000);
  }

  return (
    <div>
      <TextField
        id="outlined-number"
        label={numberAnimationStart}
        type="number"
        onChange={handleChange}
        inputProps={{ min: 0, max: 500 }}
        InputLabelProps={{
          shrink: true,
        }}
        sx={{ minWidth: "90%", backgroundColor: outlineColor }}
      />

      <ConfirmButtons
        buttonVariant="outlined"
        buttonText="Set Interval"
        widthMainButton={11}
        confirmedFunction={resetIntervalAnimation}
      />
    </div>
  );
}

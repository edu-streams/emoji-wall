import { useEffect, useState, useContext } from "react";
import { socket } from "../../services/socket";

import { Box, Typography, FormControlLabel, Switch } from "@mui/material";
import { createEmoji } from "../../functions/functions";
import { createAnimatedEmojis } from "../../functions/functionCreateAnim";
import {
  initialEmojisStat,
  defaultAnimationInterval,
} from "../../constants/constants";
import css from "./OBSLayout.module.css";

import { ThemeSwitcher } from "../../components/ThemeSwitcher";
import Lottie from "lottie-react";
const confettiAnimation = require("../../animation/103110-confetti-pop.json");

const OBSLayout = () => {
  const [emojis, setEmojis] = useState([]);
  const [emojisAnimated, setEmojisAnimated] = useState([]);
  const [pendingAnimation, setPendingAnimation] = useState(false);

  const [isDisplay, setIsDisplay] = useState(false);
  const [reactionsCount, setReactionsCount] = useState(initialEmojisStat);

  const [numberAnimation, setNumberAnimation] = useState(
    defaultAnimationInterval
  );

  const checkForAnimation = (emoji) => {
    const count = reactionsCount[emoji] + 1;
    if (count % numberAnimation === 0 && count !== 0 && !pendingAnimation) {
      setPendingAnimation(true);

      createAnimatedEmojis(emoji, setEmojisAnimated, setPendingAnimation);
      setTimeout(() => {
        setPendingAnimation(false);
      }, 2000);
      console.log(emojisAnimated);
    }
    //need for debug
    if (count % numberAnimation !== 0 && count !== 0 && !pendingAnimation) {
      setEmojisAnimated([""]);
    }
  };

  useEffect(() => {
    socket.on("messageEmojis", (msg) => {
      console.log(msg);

      const parseData = JSON.parse(msg);
      const emoji = parseData.emoji;
      setReactionsCount((prevCount) => ({
        ...prevCount,
        [emoji]: prevCount[emoji] + 1,
      }));

      createEmoji(msg, setEmojis);
    });

    socket.on("resetStats", (dataReset) => {
      if (dataReset === "reset") {
        setReactionsCount(initialEmojisStat);
      }
    });

    socket.on("setNumberAnimationStart", (dataNumber) => {
      if (dataNumber) {
        setNumberAnimation(dataNumber);
        console.log("dataNumber from OBSLayout:", dataNumber);
      }
    });

    socket.on("controlReactions", (control) => {
      setIsDisplay(control.isDisplay);
    });
  }, []);

  useEffect(() => {
    socket.on("messageEmojis", (msg) => {
      console.log(msg);

      const parseData = JSON.parse(msg);
      const emoji = parseData.emoji;

      checkForAnimation(emoji);
    });
  }, [reactionsCount, pendingAnimation]);

  return (
    <>
      <Box display={isDisplay ? "none" : "block"}>
        <Typography>OBS Layout</Typography>
        <ThemeSwitcher />
      </Box>

      <Box display={isDisplay ? "block" : "none"}>
        {/* <Box>
          {reactionsCount["🎉"] % numberAnimation === 0 &&
            reactionsCount["🎉"] !== 0 &&
            pendingAnimation && (
              <Lottie
                animationData={confettiAnimation}
                loop={false}
                style={{
                  position: "absolute",
                  top: "20%",
                  left: "60%",
                  transform: "translate(-50%, -50%)",
                }}
              />
            )}
        </Box> */}

        <Box>
          {emojis.map((emoji) => (
            <Box
              key={emoji.id}
              style={{
                top: window.innerHeight,
                left: emoji.left,
              }}
              className={css.emojiContainer}
            >
              <span className={css.emoji}>{emoji.emoji}</span>
              <span
                style={{
                  color: "#556cd6",
                }}
                className={css.emojiNick}
              >
                {emoji.user}
              </span>
            </Box>
          ))}
          {emojisAnimated.map((emojiAnim) => (
            <Box
              key={emojiAnim.id}
              style={{
                top: emojiAnim.top,
                left: emojiAnim.left,
              }}
              className={css.emojiAnimatedContainer}
            >
              <span className={css.emojiAnimated}>
                {emojiAnim.emojisAnimated}
              </span>
            </Box>
          ))}
        </Box>
      </Box>
    </>
  );
};
export default OBSLayout;

import { useEffect, useState, useContext } from "react";
import { socket } from "../../services/socket";
import {
  Box,
  ToggleButton,
  ToggleButtonGroup,
  Container,
  Typography,
} from "@mui/material";

import { ThemeSwitcher } from "../../components/ThemeSwitcher";
import { ConfirmButtons } from "../../components/ConfirmButtons";
import { AccordeonMUIForSelectors } from "../../components/AccordeonMUIForSelectors";

const OBSControl = () => {
  const [isDisplay, setIsDisplay] = useState(false);
  const [isDisplayStat, setIsDisplayStat] = useState(false);

  function resetStatFunction() {
    socket.emit("resetStats", "reset");
  }

  const handleChangeIsDisplay = (event) => {
    if (event.target.value === "false") {
      setIsDisplay(false);
    }
    if (event.target.value === "true") {
      setIsDisplay(true);
    }
  };

  const handleChangeStatVisibility = (event) => {
    if (event.target.value === "false") {
      setIsDisplayStat(false);
      socket.emit("isDisplayStats", !isDisplayStat);
    }
    if (event.target.value === "true") {
      setIsDisplayStat(true);
      socket.emit("isDisplayStats", !isDisplayStat);
    }
  };

  useEffect(() => {
    // Якщо потрібно при вмиканні віджету надсилати якісь додаткові дані окрім просто запуску віджета - дописувати їх у об'єкт
    const resultOptions = { isDisplay };

    socket.emit("controlReactions", resultOptions);
  }, [isDisplay]);

  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        textAlign: "center",
        gap: "0.5rem",
        // height: "393px",
        // width: "208px",
      }}
    >
      <Typography color="primary">OBS Control</Typography>

      <Box>
        <Typography color="primary">Widget</Typography>
        <ToggleButtonGroup
          size="small"
          color="primary"
          value={isDisplay}
          exclusive
          onChange={handleChangeIsDisplay}
          aria-label="Platform"
        >
          <ToggleButton
            style={{ minWidth: "7rem" }}
            disabled={isDisplay}
            value={true}
          >
            Enable
          </ToggleButton>
          <ToggleButton
            style={{ minWidth: "7rem" }}
            disabled={!isDisplay}
            value={false}
          >
            Disable
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>

      <Box>
        <Typography color="primary">Statistics</Typography>
        <ToggleButtonGroup
          size="small"
          color="primary"
          value={isDisplayStat}
          exclusive
          onChange={handleChangeStatVisibility}
          aria-label="Platform"
        >
          <ToggleButton
            style={{ minWidth: "7rem" }}
            disabled={isDisplayStat}
            value={true}
          >
            Show
          </ToggleButton>
          <ToggleButton
            style={{ minWidth: "7rem" }}
            disabled={!isDisplayStat}
            value={false}
          >
            Hide
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>
      <AccordeonMUIForSelectors />

      <ConfirmButtons
        buttonVariant="contained"
        buttonText="Reset Statistics"
        widthMainButton={14}
        confirmedFunction={resetStatFunction}
      />

      <ThemeSwitcher />
    </Container>
  );
};
export default OBSControl;

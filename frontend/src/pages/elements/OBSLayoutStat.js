import { useEffect, useState, useContext } from "react";
import { socket } from "../../services/socket";

import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Badge from "@mui/material/Badge";
import EmojiEmotionsTwoToneIcon from "@mui/icons-material/EmojiEmotionsTwoTone";

import { ThemeSwitcher } from "../../components/ThemeSwitcher";
import { initialEmojisStat } from "../../constants/constants";
import css from "./OBSLayout.module.css";

const OBSLayoutStat = () => {
  const [isDisplay, setIsDisplay] = useState(false);
  const [isDisplayStat, setIsDisplayStat] = useState(false);
  const [reactionsCount, setReactionsCount] = useState(initialEmojisStat);

  const totalReactionsCount = Object.values(reactionsCount).reduce(
    (acc, count) => acc + count,
    0
  );

  useEffect(() => {
    socket.on("messageEmojis", (msg) => {
      console.log(msg);

      const parseData = JSON.parse(msg);
      const emoji = parseData.emoji;
      setReactionsCount((prevCount) => ({
        ...prevCount,
        [emoji]: prevCount[emoji] + 1,
      }));
    });

    socket.on("resetStats", (dataReset) => {
      if (dataReset === "reset") {
        setReactionsCount(initialEmojisStat);
      }
    });

    socket.on("isDisplayStats", (dataDisplayStat) => {
      setIsDisplayStat(dataDisplayStat);
    });

    socket.on("controlReactions", (control) => {
      setIsDisplay(control.isDisplay);
    });
  }, []);

  return (
    <>
      <Box display={isDisplay ? "none" : "block"}>
        <Typography>OBS Layout Statistics</Typography>
        <ThemeSwitcher />
      </Box>

      <Box display={isDisplay ? "block" : "none"}>
        <Box
          display={isDisplayStat ? "flex" : "none"}
          style={{
            justifyContent: "center",
            gap: "1rem",
            position: "absolute",
            top: "20%",
            left: "0%",
          }}
        >
          {Object.entries(reactionsCount).map(
            ([emoji, count]) =>
              count > 0 && (
                <Box key={emoji}>
                  <Badge badgeContent={count} max={999} color="primary">
                    <span className={css.statEmoji}>{emoji}</span>
                  </Badge>
                </Box>
              )
          )}
          {totalReactionsCount > 0 && (
            <Box>
              <Badge
                badgeContent={totalReactionsCount}
                max={9999}
                color="primary"
              >
                <EmojiEmotionsTwoToneIcon sx={{ fontSize: 28 }} color="error" />
              </Badge>
            </Box>
          )}
        </Box>
      </Box>
    </>
  );
};
export default OBSLayoutStat;

import * as jose from "jose";
import { useState, useEffect } from "react";
import { socket } from "../../services/socket";
import { Typography, Box, Button, Container } from "@mui/material";

import { ThemeSwitcher } from "../../components/ThemeSwitcher";
import { defaultEmojisList } from "../../constants/constants";

const EventPageBlock = () => {
  const [user, setUser] = useState("");
  const [emojisList, setEmojisList] = useState(defaultEmojisList);

  function handleClick(emoji) {
    let message = JSON.stringify({ emoji: emoji, user: user });

    socket.emit("messageEmojis", message);
  }

  useEffect(() => {
    const token = localStorage.getItem("logto:asnhh0px905tmbmdwlqm1:idToken");
    if (token) {
      const claims = jose.decodeJwt(token);
      setUser(claims.name);
      console.log("User name from Reactions:", claims.name);
    }
  }, []);

  useEffect(() => {
    socket.on("resetEmojisList", (data) => {
      if (data !== defaultEmojisList) {
        setEmojisList(data);
        console.log("set List!");
      }
    });
  }, [socket]);

  return (
    <>
      <Container
        sx={{
          display: "flex",
        }}
      >
        <ThemeSwitcher
          style={{
            // mx: -3,
            position: "absolute",
            top: "0",
            left: "5px",
          }}
        />

        <Box
          sx={{
            "& button": { m: 1 },
            backgroundColor: "rgba(0, 0, 0, 0.7)",
            borderRadius: "2.3rem",
            height: "44px",
            display: "flex",
            alignItems: "center",
            margin: "auto",
          }}
        >
          {emojisList.map((emoji) => (
            <Button
              key={emoji}
              size="small"
              sx={{
                borderRadius: "50%",
                width: "28px",
                height: "28px",
                minWidth: "40px",
                minHeight: "40px",
              }}
              onClick={() => handleClick(emoji)}
            >
              <Typography sx={{ fontSize: "1.6rem" }}>{emoji}</Typography>
            </Button>
          ))}
        </Box>
      </Container>
    </>
  );
};
export default EventPageBlock;

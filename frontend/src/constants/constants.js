import { convertToEmojisStat } from "../functions/functions";

// compare with --additional-animation-time from root OBSLayout.module.css
export const additionalAnimationTime = 2000;

export const numAdditionalEmojis = 10;

export const defaultAnimationInterval = 20;

export const defaultEmojisList = [
  "💖",
  "👍",
  "🎉",
  "👏",
  "😂",
  "😮",
  "🤔",
  "😢",
  "👎",
];

export const emojisAllList = [
  "💖",
  "🎉",
  "👍",
  "👏",
  "😂",
  "😮",
  "🤔",
  "😢",
  "👎",
  "🥰",
  "😋",
  "😟",
  "😅",
  "🤣",
  "😇",
  "😚",
  "🤪",
  "😳",
  "🤩",
  "😡",
  "🥱",
  "👽",
];

export const initialEmojisStat = convertToEmojisStat(emojisAllList);

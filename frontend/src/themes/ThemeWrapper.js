import { useState, createContext } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";

import theme from "./theme";
import darkTheme from "./darkTheme";

export const ThemeContext = createContext({
  toggleTheme: () => {},
});

export const ThemeWrapper = ({ children }) => {
  const [themeMode, setThemeMode] = useState("light");

  const toggleTheme = () => {
    setThemeMode(themeMode === "light" ? "dark" : "light");
  };

  const themeDefault = createTheme(themeMode === "light" ? theme : darkTheme);

  const contextValue = {
    toggleTheme,
    themeMode,
  };

  return (
    <ThemeContext.Provider value={contextValue}>
      <ThemeProvider theme={themeDefault}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </ThemeContext.Provider>
  );
};
